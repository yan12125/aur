import json
import os
import pathlib
import subprocess
from typing import Any, Iterator
import urllib.request

SCRIPT_ROOT = pathlib.Path(__file__).parents[1].resolve()

LOCAL_REPO = pathlib.Path('/tmp/local-testing-repo')
LOCAL_REPO_SPOOL = pathlib.Path('/tmp/local-testing-repo-spool')

USERNAME = 'yan12125'

PACKAGES_BASENAME = 'packages'
AUR_PACKAGES_BASENAME = 'aur-packages'

def read_file(filename: os.PathLike[str]) -> str:
    with open(filename, 'rt') as f:
        data = f.read()

    return data

def get_official_packages() -> Iterator[dict[str, Any]]:
    resp = urllib.request.urlopen(f'https://archlinux.org/packages/search/json/?maintainer={USERNAME}')
    pkgbases = set()
    for item in json.loads(resp.read())['results']:
        pkgbase = item["pkgbase"]
        if pkgbase in pkgbases:
            continue
        yield item
        pkgbases.add(pkgbase)

def get_aur_packages() -> Iterator[dict[str, Any]]:
    # Check https://aur.archlinux.org/rpc/swagger for AUR API usage
    for by in ('maintainer', 'comaintainers'):
        resp = urllib.request.urlopen(f'https://aur.archlinux.org/rpc/v5/search/{USERNAME}?by={by}')
        for item in json.loads(resp.read())['results']:
            yield item

def _find_packages_dir(basename: str) -> pathlib.Path:
    packages_dir = None
    for candidate in (SCRIPT_ROOT.parent / basename, SCRIPT_ROOT / basename):
        if candidate.exists():
            packages_dir = candidate
            break
    assert packages_dir
    return packages_dir

def find_packages_dir() -> pathlib.Path:
    return _find_packages_dir(PACKAGES_BASENAME)

def find_aur_packages_dir() -> pathlib.Path:
    return _find_packages_dir(AUR_PACKAGES_BASENAME)

def get_pkgbuild_field(pkgbuild_file: os.PathLike[str], field: str) -> str:
    oldver = subprocess.check_output([
        'bash', '-c', f'source "{os.fspath(pkgbuild_file)}"; echo "{field}"'
    ], stderr=subprocess.DEVNULL).decode('utf-8').strip()
    return oldver
