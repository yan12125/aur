import argparse
import dataclasses
import enum
import json
import logging
import os
import pathlib
import re
import subprocess
import sys
import textwrap
import tomllib
from typing import Any, Callable, TypedDict, TypeAlias

import tomli_w

from common import (
    SCRIPT_ROOT,
    find_aur_packages_dir,
    find_packages_dir,
    get_pkgbuild_field,
    get_official_packages,
)

THIS_DIR = pathlib.Path(__file__).resolve().parent

logger = logging.getLogger('check-packages')

class Category(enum.Enum):
    NEEDS_ACTION = 1
    INFORMATIONAL = 2
    EXCEPTION = 3

class CheckerResult(TypedDict):
    title: str
    category: Category
    data: list[str]

@dataclasses.dataclass
class NewverResult:
    version: str | None = None
    error: str | None = None

# XXX: not using PEP 695 syntax, as mypy does not fully support it yet
# https://github.com/python/mypy/issues/15238
TaskFunc: TypeAlias = Callable[..., list[CheckerResult]]
PackageConfiguration: TypeAlias = dict[str, Any]
PackageConfigurations: TypeAlias = dict[str, PackageConfiguration]

def check_flagged_packages() -> list[CheckerResult]:
    my_packages = get_official_packages()

    flagged_packages = [
        f'{package["pkgbase"]} flagged at {package["flag_date"]}'
        for package in my_packages
        if package['flag_date']]

    return [{
        'title': 'Flagged package(s):',
        'category': Category.NEEDS_ACTION,
        'data': flagged_packages,
    }]

def run_nvchecker(package_configurations: PackageConfigurations) -> dict[str, NewverResult]:
    nvchecker_config = {}
    for pkgbase, package_config in package_configurations.items():
        if package_config.get('__custom_attributes__', {}).get('non_nvchecker', False):
            continue
        nvchecker_config[pkgbase] = package_config

    nvchecker_config_raw = tomli_w.dumps(nvchecker_config).encode('utf-8')

    env = os.environ.copy()
    env['OPENSSL_CONF'] = str(SCRIPT_ROOT / 'scripts' / 'openssl.cnf')
    nvchecker_output = subprocess.check_output([
        'nvchecker', '-c', '/dev/stdin', '--logger', 'both',
    ], input=nvchecker_config_raw, env=env).decode('utf-8').strip()

    newver: dict[str, NewverResult] = {}

    for nvchecker_package_info_raw in nvchecker_output.split('\n'):
        nvchecker_package_info = json.loads(nvchecker_package_info_raw)

        if nvchecker_package_info['event'] != 'updated':
            if nvchecker_package_info['event'] == 'no-result':
                newver[nvchecker_package_info['name']] = NewverResult(error=nvchecker_package_info['error'])
            continue

        newver[nvchecker_package_info['name']] = NewverResult(version=nvchecker_package_info['version'])

    return newver

def check_updated(package_configurations: PackageConfigurations, bump_version: str) -> list[CheckerResult]:
    newver: dict[str, NewverResult] = {}
    newver.update(run_nvchecker(package_configurations))
    newver.update(check_pypi_groups(package_configurations))

    packages_dir = find_packages_dir()
    aur_packages_dir = find_aur_packages_dir()

    if bump_version:
        with open(SCRIPT_ROOT / 'scripts' / f'{bump_version}-packages.toml', 'rb') as f:
            package_group = tomllib.load(f)
    else:
        package_group = {}

    updated_packages = []
    newver_failed_packages = []
    for pkg in sorted(newver.keys()):
        package_configuration = package_configurations[pkg]

        if package_configuration['source'] == 'manual' and not package_configuration.get('manual'):
            continue

        if newver[pkg].error:
            newver_failed_packages.append(f'{pkg}: {newver[pkg].error}')
            continue

        possible_pkgbuilds = [
            packages_dir / pkg / 'PKGBUILD',
            aur_packages_dir / pkg / 'PKGBUILD',
        ]
        pkgbuild_file = next(p for p in possible_pkgbuilds if os.path.exists(p))

        with open(pkgbuild_file, 'r') as pkgbuild:
            pkgbuild_content = pkgbuild.read()

        if re.search('^# UPDATE_BLOCKED:', pkgbuild_content, flags=re.MULTILINE):
            continue

        custom_attributes = package_configuration.get('__custom_attributes__', {})

        pkgbuild_field = custom_attributes.get('pkgbuild_field', '$pkgver')
        oldver = get_pkgbuild_field(pkgbuild_file, pkgbuild_field)
        if not oldver or oldver == newver[pkg].version:
            continue
        updated_packages.append(f'{pkg} {oldver} => {newver[pkg].version}')

        if custom_attributes.get('group') == bump_version or pkg in package_group.get('build_order', {}):
            if pkgbuild_field != '$pkgver':
                continue
            with open(pkgbuild_file, 'w') as pkgbuild:
                pkgbuild_content = re.sub('pkgver=.*', f'pkgver={newver[pkg].version}', pkgbuild_content)
                pkgbuild_content = re.sub('pkgrel=.*', 'pkgrel=1', pkgbuild_content)
                pkgbuild.seek(0)
                pkgbuild.write(pkgbuild_content)
                pkgbuild.truncate()

    return [{
        'title': 'Updated package(s):',
        'category': Category.NEEDS_ACTION,
        'data': updated_packages,
    }, {
        'title': 'Failed to check versions for package(s):',
        'category': Category.NEEDS_ACTION,
        'data': newver_failed_packages,
    }]

def check_pypi_group(group_name: str, group_packages: dict[str, str]) -> dict[str, NewverResult]:
    logger.info('Checking PyPI group %s', group_name)

    pypi_name_to_pkgbase = {}
    for pkgbase_, pypi_name in group_packages.items():
        pypi_name = pypi_name.split('[')[0].split('>=')[0]
        pypi_name_to_pkgbase[pypi_name] = pkgbase_

    requirements_in = '\n'.join(group_packages.values())
    # Use --python to avoid looking into older Python interpreters, which is needed after
    # https://github.com/astral-sh/uv/pull/5148
    pip_resolver_results = subprocess.check_output(
        ['uv', 'pip', 'compile', '--python=/usr/bin/python', '--no-annotate', '-'], input=requirements_in.encode('utf-8')
    ).decode('utf-8').strip()

    pkg_versions = {}

    for pip_installed_pkg in pip_resolver_results.split('\n'):
        if pip_installed_pkg.startswith('#'):
            continue

        name, version = pip_installed_pkg.split('==')

        pkgbase = pypi_name_to_pkgbase.get(name)
        if pkgbase is None:
            continue

        pkg_versions[pkgbase] = NewverResult(version=version)

    return pkg_versions

def check_pypi_groups(package_configurations: PackageConfigurations) -> dict[str, NewverResult]:
    packages_dir = find_packages_dir()

    pip_resolver_groups: dict[str, dict[str, str]] = {}
    for pkgbase, package_config in package_configurations.items():
        custom_attributes = package_config.get('__custom_attributes__', {})

        if package_config['source'] != 'pypi' or not custom_attributes.get('group'):
            continue

        group = custom_attributes['group']
        pypi_name = package_config['pypi']
        if custom_attributes.get('extras'):
            pypi_name += '[' + ','.join(custom_attributes['extras']) + ']'

        # Adding version constraints to speed up resolving
        pkgbuild_file = packages_dir / pkgbase / 'PKGBUILD'
        pkgver = get_pkgbuild_field(pkgbuild_file, '$pkgver')
        pypi_name +=  '>=' + pkgver

        if package_config.get('ignored'):
            for ignored in package_config['ignored'].split(' '):
                # https://packaging.python.org/en/latest/specifications/version-specifiers/#version-specifiers
                pypi_name += f', != {ignored}'

        pip_resolver_groups.setdefault(group, {})[pkgbase] = pypi_name

    pkg_versions = {}

    for group_name, group_packages in pip_resolver_groups.items():
        pkg_versions.update(check_pypi_group(group_name, group_packages))

    return pkg_versions

def get_package_configurations_from_dir(packages_dir: pathlib.Path, skip_missing: bool = False) -> PackageConfigurations:
    package_configurations = {}

    for package_dir in packages_dir.iterdir():
        if package_dir.name in ('.repo',) or package_dir.name.endswith('-git'):
            continue

        per_package_nvchecker_file = package_dir / '.nvchecker.toml'

        try:
            with open(per_package_nvchecker_file, 'rb') as f:
                data = tomllib.load(f)
                cur_packages = list(data.keys())
                # Package configurations in .nvchecker.toml may be commented/disabled
                assert not cur_packages or len(cur_packages) == 1 and cur_packages[0] == package_dir.name
                package_configurations.update(data)
        except FileNotFoundError:
            if skip_missing:
                continue
            raise

    return package_configurations

def get_package_configurations() -> PackageConfigurations:
    packages_dir = find_packages_dir()
    aur_packages_dir = find_aur_packages_dir()

    package_configurations = get_package_configurations_from_dir(packages_dir)
    package_configurations.update(get_package_configurations_from_dir(aur_packages_dir, skip_missing=True))

    return package_configurations

def main() -> int:
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument('--bump-version', type=str, default='')
    args = parser.parse_args()

    all_checker_result_categories = set()

    package_configurations = get_package_configurations()

    tasks: list[tuple[TaskFunc, tuple[Any, ...]]] = [
        (check_updated, (package_configurations, args.bump_version)),
        (check_flagged_packages, ()),
    ]

    outputs = ''
    for task, task_args in tasks:
        try:
            results = task(*task_args)
            for result in results:
                if not result['data']:
                    continue
                if result['category'] == Category.NEEDS_ACTION:
                    all_checker_result_categories.add(Category.NEEDS_ACTION)
                outputs += result['title'] + '\n'
                outputs += textwrap.indent('\n'.join(result['data']), '  ') + '\n'
        except Exception as exc:
            logger.exception('Task %s raises an exception:', task.__name__, exc_info=(type(exc), exc, exc.__traceback__))
            all_checker_result_categories.add(Category.EXCEPTION)

    print(outputs, end='')

    if Category.EXCEPTION in all_checker_result_categories:
        # Matches the exit code of Py_Main when there is an exception
        # https://docs.python.org/3/c-api/veryhigh.html
        return 1
    elif Category.NEEDS_ACTION in all_checker_result_categories:
        # Not using 2 as it's for invalid command line arguments
        return 3
    else:
        return 0

if __name__ == '__main__':
    sys.exit(main())
