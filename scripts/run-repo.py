import os
import tarfile

from common import LOCAL_REPO, LOCAL_REPO_SPOOL, SCRIPT_ROOT

def main() -> None:
    LOCAL_REPO.mkdir(exist_ok=True)
    LOCAL_REPO_SPOOL.mkdir(exist_ok=True)

    for arch in ('any', 'x86_64'):
        arch_path = LOCAL_REPO / arch
        arch_path.mkdir(exist_ok=True)
        db_path = arch_path / 'local-testing.db.tar.gz'
        if not db_path.exists():
            # archrepo2 does not create a dbwithout adding packages first
            # Do it manually so the new repo can be used immediately
            with tarfile.open(db_path, 'w:gz'):
                pass
            os.symlink('local-testing.db.tar.gz', arch_path / 'local-testing.db')
    os.execvp('archreposrv', ('archreposrv', SCRIPT_ROOT / 'scripts' / 'archrepo2.ini'))

if __name__ == '__main__':
    main()
