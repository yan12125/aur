import jinja2

from common import (
    SCRIPT_ROOT,
    get_aur_packages,
    get_official_packages,
)

def main() -> None:
    packages = get_official_packages()
    aur_packages = get_aur_packages()

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(SCRIPT_ROOT / 'scripts'), autoescape=True)

    template = env.get_template('default.xml.j2')
    content = template.render(projects=[package['pkgbase'] for package in packages])

    aur_template = env.get_template('default-aur.xml.j2')
    aur_content = aur_template.render(projects=[package['Name'] for package in aur_packages])

    with open(SCRIPT_ROOT / 'default.xml', 'w') as f:
        f.write(content)

    with open(SCRIPT_ROOT / 'default-aur.xml', 'w') as f:
        f.write(aur_content)

if __name__ == '__main__':
    main()
