import configparser
import os.path
import re
import sys
import urllib.parse

conf = configparser.ConfigParser()
conf.read(sys.argv[1])
src_array = ''
prepare_steps = ''
for sec in conf.sections():
    mobj = re.match(r'^submodule "([^"]+)"$', sec)
    assert mobj is not None
    submodule_name = mobj.group(1)
    url = conf[sec]['url']
    url = url.replace('git://', 'https://')
    path = urllib.parse.urlparse(url).path
    basename = os.path.basename(path)
    if basename.endswith('.git'):
        basename = basename[:-len('.git')]
    src_array += f'"git+{url}"\n'
    prepare_steps += f'git config submodule.{submodule_name}.url "$srcdir/{basename}"\n'

print(src_array)
print(prepare_steps)
