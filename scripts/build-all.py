import argparse
import logging
import os
import pathlib
import shutil
import subprocess
import time
import tomllib
from typing import Any

from common import LOCAL_REPO, LOCAL_REPO_SPOOL, SCRIPT_ROOT

logger = logging.getLogger('build-all')

def main() -> None:
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument('--group', type=str, required=True)
    parser.add_argument('--move', action='store_true', default=False)
    parser.add_argument('--release', action='store_true', default=False)
    parser.add_argument('topdir')
    args = parser.parse_args()

    group = args.group

    with open(SCRIPT_ROOT / 'scripts' / f'{group}-packages.toml', 'rb') as f:
        group_packages = tomllib.load(f)
        build_order = group_packages['build_order']
        make_flags = group_packages.get('make_flags')

    topdir = pathlib.Path(args.topdir).resolve()
    with open(f'{group}-build.out', 'a') as log_out, open(f'{group}-build.err', 'a') as log_err:
        for package in build_order:
            pkgdir = topdir / package

            # TODO: figure out another condition - if `pkgctl release` fails, it's possible to have a clean tree but with local (not pushed) commits
            git_diff = subprocess.check_output(['git', 'diff'], cwd=pkgdir).strip()
            if not git_diff:
                logger.info('No git diff found for package %s, skipping...', package)
                continue

            if list(pkgdir.glob('*.pkg.tar.zst')):
                logger.info('Package %s already built, skipping...', package)
                continue

            logger.info('Building %s', package)

            env = os.environ.copy()
            if make_flags:
                env['MAKEFLAGS'] = make_flags

            # Use dict[str, Any] as a work-around for https://github.com/python/mypy/issues/5382
            subprocess_args: dict[str, Any] = {'cwd': pkgdir, 'stdout': log_out, 'stderr': log_err, 'env': env}
            subprocess.check_call(['updpkgsums'], **subprocess_args)
            subprocess.check_call(['local-testing-x86_64-build', '--', '-D', LOCAL_REPO], **subprocess_args)
            for built_package in pkgdir.glob('*.pkg.tar.zst'):
                shutil.copy2(built_package, LOCAL_REPO_SPOOL / built_package.name)
            # wait for archreposrv; see wait-time in ./archrepo2.ini
            time.sleep(3)

    if args.release:
        for package in build_order:
            pkgdir = topdir / package
            git_diff = subprocess.check_output(['git', 'diff'], cwd=pkgdir).strip()
            if not git_diff:
                continue
            if not list(pkgdir.glob('*.pkg.tar.zst')):
                continue
            subprocess.check_call(['pkgctl', 'release', '--testing', '--message', 'bump'], cwd=pkgdir)
        subprocess.check_call(['pkgctl', 'db', 'update'])

    if args.move:
        # TODO: move only updated packages; using state repo to check if packages are in testing? https://gitlab.archlinux.org/archlinux/packaging/state
        packages_to_move = build_order
        subprocess.check_call(['pkgctl', 'db', 'move', 'extra-testing', 'extra'] + packages_to_move)

if __name__ == '__main__':
    main()
