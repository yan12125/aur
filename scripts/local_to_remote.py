import argparse
import os
import os.path
import pathlib
import shutil
import subprocess
import sys
from collections.abc import Collection

import pygit2
from pygit2.enums import FileStatus

from common import get_pkgbuild_field, read_file


default_repo_name = 'yan12125'


class MyCallbacks(pygit2.RemoteCallbacks):
    # Ignore no-untyped-def as there is no good way for annotate forwarded *args and **kwargs for now
    # https://github.com/python/typing/discussions/1079
    def __init__(self, *args, **kwargs) -> None:  # type: ignore[no-untyped-def]
        super().__init__(*args, **kwargs)
        self._error_occurred = False

    def credentials(self, *args, **kwargs) -> pygit2.KeypairFromAgent:  # type: ignore[no-untyped-def]
        return pygit2.KeypairFromAgent('aur')

    def push_update_reference(self, refname: str, message: str) -> None:
        # Needed as "When the remote has a githook installed, that
        # denies the reference this function will return successfully." [1]
        # The message is incomplete, though. [2]
        # [1] https://www.pygit2.org/remotes.html
        # [2] https://github.com/libgit2/pygit2/issues/844
        if message is not None:
            print(f'Failed to push to {refname}: {message}')
            self._error_occurred = True

    @property
    def error_occurred(self) -> bool:
        return self._error_occurred

my_callbacks = MyCallbacks()

def dump_new_files(remote_path: pathlib.Path, files: Collection[str]) -> None:
    print('There are %d new files:' % len(files))
    for filename in files:
        print('==> ' + filename)
        print(read_file(remote_path / filename))

def update_remote_repo(remote_path: pathlib.Path, commit_message: str, watched_files: set[str], user_name: str, user_email: str) -> int:
    remote_repo = pygit2.Repository(remote_path)

    print('Generating .SRCINFO')
    with open(os.path.join(remote_path, '.SRCINFO'), 'wb') as f:
        subprocess.check_call([
            'makepkg', '--printsrcinfo'], cwd=remote_path, stdout=f)

    watched_files.add('.SRCINFO')

    if remote_repo.head_is_unborn:
        dump_new_files(remote_path, watched_files)
    else:
        new_files = []
        remote_repo_status = remote_repo.status(untracked_files='all')
        for filename, file_status in remote_repo_status.items():
            if file_status == FileStatus.WT_NEW: # new file in a working tree
                new_files.append(filename)
        patch = remote_repo.diff().patch

        if not patch and not new_files:
            print('No changes found!')
            return 3

        print(patch)
        dump_new_files(remote_path, new_files)

    print('Commit message: ')
    print(commit_message)

    ans = input('Committed? ')
    if ans != 'yes':
        return 2

    committer = pygit2.Signature(user_name, user_email)
    author = pygit2.Signature(user_name, user_email)

    tree = remote_repo.TreeBuilder()
    remote_repo.index.read()
    for filename in watched_files:
        blob = remote_repo.create_blob_fromworkdir(filename)
        tree.insert(
            filename, blob,
            os.stat(os.path.join(remote_path, filename)).st_mode)
        remote_repo.index.add(filename)

    # handle deleted files
    for filename in os.listdir(remote_path):
        if filename not in watched_files:
            if filename == '.git':
                continue
            remote_repo.index.remove(filename)
            os.remove(os.path.join(remote_path, filename))

    remote_repo.index.write()

    tree_oid = tree.write()

    if remote_repo.head_is_unborn:
        parents = []
    else:
        parents = [remote_repo.head.target]
    new_commit = remote_repo.create_commit(
        'refs/heads/master', author, committer,
        commit_message, tree_oid, parents)
    print('Committed as %s' % new_commit)

    ans = input('Push to AUR? ')
    if ans != 'yes':
        return 4

    remote_repo.remotes['origin'].push(
        ['refs/heads/master'], callbacks=my_callbacks)
    if my_callbacks.error_occurred:
        return 5

    return 0

def clone_remote_repo(package_name: str, remote_package_path: os.PathLike[str]) -> None:
    if not os.path.exists(remote_package_path):
        print('Cloning %s.git...' % package_name)
        pygit2.clone_repository(
            'aur@aur.archlinux.org:/%s.git' % package_name,
            remote_package_path, callbacks=my_callbacks)


def get_last_commit(repo: pygit2.Repository, package_name: str) -> pygit2.Commit:
    for commit in repo.walk(repo.head.target):
        assert len(commit.parents) == 1

        changed = repo.diff(commit.parents[0], commit)
        changed_packages = set()
        for delta in changed.deltas:
            changed_packages.add(os.path.basename(os.path.dirname(delta.new_file.path)))
            changed_packages.add(os.path.basename(os.path.dirname(delta.old_file.path)))

        if package_name not in changed_packages:
            continue

        return commit


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('--keep-pkgrel', action='store_true', default=False)
    parser.add_argument('package_path')
    args = parser.parse_args()

    local_package_path = pathlib.Path(args.package_path)
    package_name = local_package_path.name
    repo_path = local_package_path.parent

    root_path = pathlib.Path(__file__).resolve().parents[1]
    local_package_path = repo_path / package_name

    main_repo = pygit2.Repository(str(repo_path))
    abort = False
    for file_path, status in main_repo.status().items():
        if status != pygit2.GIT_STATUS_IGNORED and file_path.startswith(package_name + '/'):
            print('Unclean file %s found' % file_path)
            abort = True
    if abort:
        return 1

    last_commit = get_last_commit(main_repo, package_name)

    main_index = main_repo.index
    main_index.read()
    watched_files = set()

    for entry in main_index:
        cur_path = entry.path
        if cur_path.startswith('archlinuxcn/'):
            cur_path = cur_path[len('archlinuxcn/'):]
        if cur_path.startswith('%s/' % package_name):
            filename = cur_path[len(package_name) + 1:]
            if filename.startswith(f'{default_repo_name}/'):
                filename = filename[len(default_repo_name) + 1:]
            watched_files.add(filename)

    target_local_path = local_package_path / package_name
    if not os.path.isdir(target_local_path):
        target_local_path = local_package_path
    subprocess.check_call(['updpkgsums'], cwd=target_local_path)
    remote_package_path = root_path / 'aur-remote' / package_name
    clone_remote_repo(package_name, remote_package_path)

    for f in ('lilac.py', 'lilac.yaml', 'package.list'):
        try:
            watched_files.remove(f)
        except KeyError:
            pass

    if args.keep_pkgrel:
        remote_pkgbuild_path = remote_package_path / 'PKGBUILD'
        orig_pkgrel = get_pkgbuild_field(remote_pkgbuild_path, '$pkgrel')

    for filename in watched_files:
        print('Copying %s ...' % filename)
        local_file = os.path.join(target_local_path, filename)
        if not os.path.exists(local_file):
            shutil.copyfile(
                os.path.join(local_package_path, filename), local_file)
        shutil.copyfile(
            local_file, os.path.join(remote_package_path, filename))

    if args.keep_pkgrel:
        subprocess.check_call(['sed', '-i', f's#pkgrel=.*#pkgrel={orig_pkgrel}#', remote_pkgbuild_path])

    return update_remote_repo(
        remote_package_path, last_commit.message.strip(), watched_files,
        last_commit.committer.name, last_commit.committer.email)


if __name__ == '__main__':
    sys.exit(main())
