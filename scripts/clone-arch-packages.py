import subprocess

from common import (
    AUR_PACKAGES_BASENAME,
    PACKAGES_BASENAME,
    SCRIPT_ROOT,
    get_official_packages,
    get_aur_packages,
)

def git_url_for_package(pkg: str) -> str:
    return f'https://gitlab.archlinux.org/archlinux/packaging/packages/{pkg}.git'

def git_url_for_aur_package(pkg: str) -> str:
    return f'https://aur.archlinux.org/{pkg}.git'

def main() -> None:
    # Clone official packages
    packages_dir = SCRIPT_ROOT / PACKAGES_BASENAME

    packages_dir.mkdir(exist_ok=True)

    for package in get_official_packages():
        pkg = package['pkgbase']
        subprocess.check_call(['git', 'clone', git_url_for_package(pkg)], cwd=packages_dir)

    # Clone AUR packages
    aur_packages_dir = SCRIPT_ROOT / AUR_PACKAGES_BASENAME

    aur_packages_dir.mkdir(exist_ok=True)

    for package in get_aur_packages():
        pkg = package['Name']
        if pkg.endswith('-git'):
            continue
        subprocess.check_call(['git', '-c', 'init.defaultBranch=master', 'clone', git_url_for_aur_package(pkg)], cwd=aur_packages_dir)

if __name__ == '__main__':
    main()
